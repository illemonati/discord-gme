/**
 * @name DiscordGMEPlugin
 * @description Puts gme into discord
 * @author illemonati
 * @version 2022.11.03
 */
Object.defineProperty(exports, "__esModule", { value: true });
class DiscordGMEPlugin {
    start() {
        console.log("Loading DiscordGMEPlugin");
        window.addEventListener("resize", this.posDiv);
    }
    posDiv() {
        positionDiv();
    }
    stop() {
        console.log("Stopping DiscordGME");
        window.removeEventListener("resize", this.posDiv);
    }
    onSwitch() {
        console.log("Switched view");
        injectGME();
    }
}
exports.default = DiscordGMEPlugin;
const injectAnchor = (chatInnerDiv) => {
    const gmeAnchor = document.createElement("div");
    gmeAnchor.classList.add("gme-anchor");
    gmeAnchor.style.width = "35rem";
    gmeAnchor.style.marginTop = "-20rem";
    gmeAnchor.style.height = "20rem";
    gmeAnchor.style.position = "sticky";
    gmeAnchor.style.bottom = "2rem";
    gmeAnchor.style.left = "calc(80% - 2rem)";
    chatInnerDiv.appendChild(gmeAnchor);
    return gmeAnchor;
};
const initializeGraph = async () => {
    const gmeDiv = document.createElement("div");
    const tvScript = document.createElement("script");
    gmeDiv.style.position = "absolute";
    gmeDiv.style.zIndex = "10000000";
    gmeDiv.style.background = "rgba(0, 0, 0, 0)";
    gmeDiv.style.opacity = "80%";
    gmeDiv.classList.add("gme-div");
    gmeDiv.id = "gme-div";
    tvScript.src = "https://s3.tradingview.com/tv.js";
    document.body.appendChild(tvScript);
    document.body.appendChild(gmeDiv);
    await new Promise((r) => (tvScript.onload = r));
    const tradingViewWidget = new TradingView.widget({
        autosize: true,
        symbol: "NYSE:GME",
        interval: "5",
        timezone: "America/New_York",
        theme: "dark",
        style: "1",
        locale: "en",
        toolbar_bg: "#f1f3f6",
        enable_publishing: false,
        allow_symbol_change: true,
        container_id: "gme-div",
    });
    return gmeDiv;
};
const positionDiv = (gmeDiv, anchor) => {
    const anc = anchor || document.querySelector(".gme-anchor");
    const div = gmeDiv || document.querySelector(".gme-div");
    const position = anc.getBoundingClientRect();
    div.style.left = `${position.left}px`;
    div.style.top = `${position.top}px`;
    div.style.width = `${position.width}px`;
    div.style.height = `${position.height}px`;
};
const injectGME = async () => {
    const chatInnerDiv = document.querySelector(".scroller-kQBbkU");
    if (!chatInnerDiv)
        return;
    chatInnerDiv.style.position = "relative";
    const anchor = injectAnchor(chatInnerDiv);
    const gmeDiv = document.querySelector(".gme-div") ||
        (await initializeGraph());
    positionDiv(gmeDiv, anchor);
    new ResizeObserver(() => positionDiv(gmeDiv, anchor)).observe(chatInnerDiv);
};
